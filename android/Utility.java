package com.tealeaf.plugin.plugins;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.tealeaf.EventQueue;
import com.tealeaf.logger;
import com.tealeaf.plugin.IPlugin;

/**
 * @author jsdevlover
 *
 */
public class Utility extends Activity implements IPlugin {

    private Activity _activity;
    private Context _ctx;
    public Utility() {
    }

    public void onCreateApplication(Context applicationContext) {
        logger.log("in onCreateApplication method..");
        _ctx = applicationContext;
    }

    public void onCreate(Activity activity, Bundle savedInstanceState) {
        logger.log("in onCreate method..");
        _activity = activity;
    }

    public void onResume() {
        logger.log("in onResume method..");
    }

    public void onStart() {
        logger.log("in onStart method..");
    }

    public void onPause() {
    }

    public void onStop() {
        logger.log("in onStop method..");
    }

    public void onDestroy() {
    }

    public void onNewIntent(Intent intent) {
    }

    public void setInstallReferrer(String referrer) {
    }

    public void onActivityResult(Integer request, Integer result, Intent data) {
    }

    private void showGPSDisabledAlertToUser() {
    }

    public void logError(String error) {
    }

    public void onBackPressed() {
    }

    public class ResponseEvent extends com.tealeaf.event.Event {
        String errorMessage;
        String data;
        String token;

        public ResponseEvent(String errorMessage, String data, String token) {
            super(Utility.class.getSimpleName() + "ResponseEvent");
            this.errorMessage = errorMessage;
            this.data = data;
            this.token = token;
        }
    }

    /**
     * Gets device details.
     * @param jsonData
     */
    public void getDeviceDetails(String jsonData){
        logger.log("getDeviceDetails(String jsonData): jsonData = " + jsonData);
        String token = "";
        JSONObject responseJsonObj = new JSONObject();

        try {
            token  =  new JSONObject(jsonData).optString("token");
            TelephonyManager telephonyManager = (TelephonyManager)_ctx.getSystemService(Context.TELEPHONY_SERVICE);
            String imei = telephonyManager.getDeviceId();
            String deviceSoftwareVersion = telephonyManager.getDeviceSoftwareVersion();
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            String networkOperator = telephonyManager.getNetworkOperator();
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            int networkType = telephonyManager.getNetworkType();
            int phoneType = telephonyManager.getPhoneType();
            String sim = telephonyManager.getSimSerialNumber();
            String phone = "";
            try {
                phone =tMgr.getLine1Number();
            }catch(NullPointerException ex){

            }
            if(phone.equals("")){
                phone = telephonyManager.getSubscriberId();
            }

            responseJsonObj.put("imei", imei);
            responseJsonObj.put("deviceSoftwareVersion", deviceSoftwareVersion);
            responseJsonObj.put("networkCountryIso", networkCountryIso);
            responseJsonObj.put("networkOperator", networkOperator);
            responseJsonObj.put("networkOperatorName", networkOperatorName);
            responseJsonObj.put("networkType", networkType);
            responseJsonObj.put("phoneType", phoneType);
            responseJsonObj.put("sim", sim);
            responseJsonObj.put("phone", phone);

            logger.log("Device details: "+ responseJsonObj.toString());
            EventQueue.pushEvent(new ResponseEvent("", responseJsonObj.toString(), token));
        } catch (JSONException e) {
            EventQueue.pushEvent(new ResponseEvent("JSONException:" + e.getMessage()+" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        }
    }

    /**
     * Sends message
     * @param jsonData
     */
    public void sendMessage(String jsonData) {
        logger.log("sendMessage(String jsonData): jsonData = "+ jsonData);
        String token = "";
        try {
            JSONObject jsonObj = new JSONObject(jsonData);
            String message = jsonObj.optString("msg");
            JSONObject jsonOpt = jsonObj.optJSONObject("options");
            String toAddress = jsonOpt.optString("toAddress");
            token =  jsonObj.optString("token");
            JSONObject responseJsonObj = new JSONObject();

            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            //        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "message subject");
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);
            shareIntent.putExtra("address",toAddress);

            _activity.startActivity(Intent.createChooser(shareIntent, "Send Message by"));
            finish();
            responseJsonObj.put("isSent", true);
            logger.log("Msg sent to"+toAddress+" successfull");
            EventQueue.pushEvent(new ResponseEvent("", responseJsonObj.toString(), token));
        } catch (JSONException e) {
            EventQueue.pushEvent(new ResponseEvent("JSONException:" + e.getMessage()+" [[STACK]] :"+stackTraceToString(e), "", token));
            logger.log(e);
            return;
        }

    }

    private static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

}

