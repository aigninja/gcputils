# Game Closure Devkit Plugin: gcputils

A plugin with miscellaneous utility methods.

Utility APIs:

####gcpUtils.getDeviceDetails()
Returns the device details in the form of following JSON:
```json
{
  "imei": "XXXXXXXXXXXXXXX",
  "phone": "",
  "sim": "XXXXXXXXXXXXXXXXXXXX",
  "name": "deviceDetails",
  "priority": 0
}
```


