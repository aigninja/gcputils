(function (global) {
    var callbackStore = {},
        apiClassName = "Utility",
        apis = 'getDeviceDetails sendMessage'.split(' ');

    global.gcpUtils = {};

    function createAPI(api) {
        return function (input, callback) {
            if (!input || typeof input !== "object") {
                input = {};
            }
            input.token = Date.now() + Math.random().toString(36).substr(2, 9);
            logger.log('Making a call to ' + api + ' API of GCP Utils with input ' + JSON.stringify(input));
            NATIVE.plugins.sendEvent(apiClassName, api, JSON.stringify(input));
            if (typeof callback === "function") {
                callbackStore[input.token] = callback;
            }
        };
    }

    apis.forEach(function (api) {
        global.gcpUtils[api] = createAPI(api);
    });

    NATIVE.events.registerHandler(apiClassName + 'ResponseEvent', function (response) {
        logger.log(apiClassName + ' response handler, response: ' + JSON.stringify(response));
        if (response.errorMessage) {
            callbackStore[response.token](new Error(response.errorMessage));
        } else {
            callbackStore[response.token](null, JSON.parse(response.data));
        }
        delete callbackStore[response.token];
    });


}(GLOBAL));
